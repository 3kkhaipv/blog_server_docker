#!/bin/bash
set -eu

echo -e "\n==========> start blog setup\n"
docker-compose exec --user=blog web sh -c 'cd /var/www/html/blog_server_api && composer install'
docker-compose exec web sh -c "sed -ie 's/post_max_size = .*/post_max_size = 100M/' /etc/php.ini"
docker-compose exec web sh -c "sed -ie 's/upload_max_filesize = .*/upload_max_filesize = 100M/' /etc/php.ini"
echo -e "\n==========> blog setup finish.\n"
