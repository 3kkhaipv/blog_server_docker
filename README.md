# Blog Docker Setting

## Prequisites

- Docker. Visit [installation guide](https://www.docker.com/community-edition)
- Docker Compose. Visit [installation guide](https://docs.docker.com/compose/install/)


## Preparation

- Make sure cloning 3 repositories into the same directory

```
... blog_server_docker/
... blog_server_api/
... blog_server_admin/
```


## Builder Docker Images Steps:

### 1. Create .env file by copying .env.example

```
$ cp .env.example .env
```

### 2.  Build docker containers

```
$ docker-compose build
```
```
Note: Build docker containers clear cache
$ docker-compose build --no-cache
```

### 3. Run docker containers

```
$ docker-compose up -d

Note: Need shutdown apache2 and mysql.
$ sudo service apache2 stop
$ sudo service mysql stop
```

> -d: detach mode

### 4. Setup blog project with `post_max_size` and `upload_max_filesize`

```
$ sh web/setup.sh
```

### 5. Access to web container as "blog" user

```
$ docker-compose exec --user=blog web bash
```

### 6. Add to virtual host

```
127.0.0.1 blog-adminlocal.net
127.0.0.1 blog-apilocal.net
127.0.0.1 blog-maildevlocal.net
```
### 7. Access to site

```
// Site admin
blog-adminlocal.net

// Site api
blog-apilocal.net

// Site maildev
localhost:1080

// Site minio(S3 local)
localhost:9000

Note: Input to login
Access Key: access
Secrect Key: secretkey

// Site adminer
localhost:4567

Note: Input to login db_core
System: PostgreSQL
Server: db_core
Username: blog
Password: password
Database: db_blog
```
